package com.famous.coader.firebasegoogleauth

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth

class HomeActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

       var logout = findViewById<Button>(R.id.signOut)
        logout.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            FirebaseAuth.getInstance().signOut()
        }
    }
}